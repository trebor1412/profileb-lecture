﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InheritanceConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var myForce = new Force();


            myForce.CreateBuilding(BuildingTypeEnum.Church);
            myForce.CreateBuilding(BuildingTypeEnum.Farm);
            myForce.CreateBuilding(BuildingTypeEnum.TrainingCenter);
            myForce.CreatePerson();
            myForce.ListProperties(false);
            myForce.ListProperties(true);
        }
    }
}