using System;

namespace InheritanceConsole
{
    public abstract class Building : Unit
    {       
        public virtual Person CreatePerson() => null;
    }
}