namespace AOE {
    public class Building {
        public int Id { get; set; }

        public BuildingEnum Type { get; set; }

        public Person CreatePerson () {
            switch (Type) {
                case BuildingEnum.Farm:
                    return new Person { Job = JobEnum.Farmer };
                case BuildingEnum.Church:
                    return new Person { Job = JobEnum.Monk };
                case BuildingEnum.TrainingCenter:
                    return new Person { Job = JobEnum.Knight };
                default:
                    return null;
            }
        }
    }
}