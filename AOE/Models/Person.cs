namespace AOE {
    public class Person {
        public int Id { get; set; }

        /// <summary>
        /// 職業
        /// </summary>
        /// <value></value>
        public JobEnum Job { get; set; }

        public Building CreateBuilding (BuildingEnum buildingType) {
            if (Job != JobEnum.Farmer) {
                return null;
            }

            return new Building { Type = buildingType };
        }
    }
}