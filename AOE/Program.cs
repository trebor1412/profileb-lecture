﻿using System;
using System.Linq;

namespace AOE
{
    class Program
    {
        static void Main(string[] args)
        {
            var force = new Force { Name = "Trebor" };
            force.CreateBuilding(BuildingEnum.Farm);
            force.CreateBuilding(BuildingEnum.Church);
            force.CreateBuilding(BuildingEnum.TrainingCenter);
            force.CreatePerson(JobEnum.Knight, 4);
            force.CreatePerson(JobEnum.Monk, 3);
            force.CreatePerson(JobEnum.Farmer, 2);
            

            Console.WriteLine($@"
            {force.Name} has :
            {force.PeopleCount(JobEnum.Farmer)} farmers 
            {force.PeopleCount(JobEnum.Monk)} monks 
            {force.PeopleCount(JobEnum.Knight)} knights 
            {force.BuildingCount(BuildingEnum.Farm)} farms 
            {force.BuildingCount(BuildingEnum.Church)} churchs 
            {force.BuildingCount(BuildingEnum.TrainingCenter)} trainingcenter");
        }
    }
}
