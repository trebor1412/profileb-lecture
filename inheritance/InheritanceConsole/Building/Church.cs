namespace InheritanceConsole
{
    public class Church : Building
    {
        public override Person CreatePerson() => new Monk{};
    }
}