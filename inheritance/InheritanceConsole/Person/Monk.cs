using System;

namespace InheritanceConsole
{
    public class Monk : Person
    {
        public override void Attack() => Console.WriteLine("用木杖攻擊");
    }
}