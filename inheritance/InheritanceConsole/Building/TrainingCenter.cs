namespace InheritanceConsole
{
    public class TrainingCenter : Building
    {
        public override Person CreatePerson() => new Knight();
    }
}