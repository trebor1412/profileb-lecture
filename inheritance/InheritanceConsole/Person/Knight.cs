using System;

namespace InheritanceConsole
{
    public class Knight : Person
    {
        public override void Attack() => Console.WriteLine("用長槍攻擊");
    }
}