using System;
using System.Collections.Generic;
using System.Linq;

namespace AOE {
    public class Force {
        public Force () {
            People.Add (new Person { Job = JobEnum.Farmer });
        }

        public string Name { get; set; }

        /// <summary>
        /// 人口
        /// </summary>
        /// <value></value>
        public List<Person> People { get; set; } = new List<Person> { };

        /// <summary>
        /// 建築物
        /// </summary>
        /// <value></value>
        public List<Building> Buildings { get; set; } = new List<Building> { };

        public int BuildingCount (BuildingEnum type) {
            return Buildings.Where (x => x.Type == type).Count ();
        }

        public int PeopleCount (JobEnum type) {
            return People.Where (x => x.Job == type).Count ();
        }

        public void CreateBuilding (BuildingEnum type) {
            var farmer = People.FirstOrDefault (x => x.Job == JobEnum.Farmer);
            if (farmer != null) {
                Buildings.Add (farmer.CreateBuilding (type));
            } else {
                Console.WriteLine ("We have no farmer");
            }
        }

        public void CreatePerson (JobEnum job, int count) {
            Building myBuilding = null;
            switch (job) {
                case JobEnum.Farmer:
                    myBuilding = Buildings.FirstOrDefault (x => x.Type == BuildingEnum.Farm);
                    break;
                case JobEnum.Monk:
                    myBuilding = Buildings.FirstOrDefault (x => x.Type == BuildingEnum.Church);
                    break;
                case JobEnum.Knight:
                    myBuilding = Buildings.FirstOrDefault (x => x.Type == BuildingEnum.TrainingCenter);
                    break;
                default:
                    break;
            }
            if (myBuilding != null) {
                for (int i = 0; i < count; i++) {
                    People.Add (myBuilding.CreatePerson ());
                }
            } else {
                Console.WriteLine ($"We can not create {job}");
            }

        }

    }
}