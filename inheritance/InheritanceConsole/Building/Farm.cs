namespace InheritanceConsole
{
    public class Farm : Building
    {
        public override Person CreatePerson() => new Farmer{};
    }
}