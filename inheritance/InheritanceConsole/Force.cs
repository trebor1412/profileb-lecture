using System;
using System.Collections.Generic;
using System.Linq;

namespace InheritanceConsole
{
    public class Force
    {
        public Force()
        {
            People.Add(new Farmer());
        }
        
        public List<Person> People { get; set; } = new List<Person>();

        public List<Building> Buildings { get; set; } = new List<Building>();

        public Farmer MyFarmer
        {
            get
            {
                var farmer = People.FirstOrDefault(x => x is Farmer);
                return farmer as Farmer;
            }
        }

        public void AllAttack()
        {
            People.ForEach(x => x.Attack());
        }

        public void CreatePerson()
        {
            Buildings.ForEach(x =>
            {
                People.Add(x.CreatePerson());
            });
        }

        public void CreateBuilding(BuildingTypeEnum type)
        {
            Buildings.Add(MyFarmer.CreateBuilding(type));
        }

        public void ListProperties(bool listPerson)
        {
            Console.WriteLine(listPerson ? "我有這些人" : "我有這些建築物");
            List<Unit> myUnits = new List<Unit>();

            if (listPerson)
            {
                People.ForEach(x => myUnits.Add(x));
            }
            else
            {
                Buildings.ForEach(x => myUnits.Add(x));
            }
            myUnits.ForEach(x => { Console.WriteLine(x.GetType().Name); });
        }
    }
}