using System;

namespace InheritanceConsole
{
    public abstract class Unit
    {
        public Guid ID => Guid.NewGuid();
    }
}